package main

import (
	"fmt"
	"gitlab.com/guglicap/gomcsrv/pkg/anvil"
	"log"
)

func checkErr(err error, fatal bool) {
	if err != nil {
		if fatal {
			log.Fatal(err)
		}
		log.Println(err)
	}
}

func main() {
	world := anvil.OpenDimension("./region")
	for x := 0; x <= 4; x++ {
		for z := -1; z >= -5; z-- {
			id, _ := world.Block(x, 3, z)
			fmt.Printf("block at (%d, %d): %s\n", x, z, id)
		}
	}
}
