package login

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/gob"
	"os"
)

// KeyHelper is an helper struct which deals with encryption keys.
// It handles generation, storage and loading.
type KeyHelper struct {
	privKey     *rsa.PrivateKey
	pubKey      *rsa.PublicKey
	keyFileName string
}

// NewKeyHelper returns a KeyHelper which will try to load the specified server private key, located at keyFileName.
// If the key can't be found it will generate it and try to save it. Make sure to have write permissions for the directory.
func NewKeyHelper(keyFileName string) *KeyHelper {
	return &KeyHelper{
		keyFileName: keyFileName,
	}
}

func (kh *KeyHelper) generate() (err error) {
	kh.privKey, err = rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return
	}
	kh.pubKey = &kh.privKey.PublicKey
	return
}

func (kh *KeyHelper) load() (err error) {
	privKeyFile, err := os.OpenFile(kh.keyFileName, os.O_RDONLY, 0644)
	if err != nil {
		return
	}
	defer privKeyFile.Close()
	err = gob.NewDecoder(privKeyFile).Decode(&kh.privKey)
	if err != nil {
		return
	}
	kh.pubKey = &kh.privKey.PublicKey
	return
}

func (kh *KeyHelper) save() (err error) {
	privKeyFile, err := os.Create(kh.keyFileName)
	if err != nil {
		return
	}
	defer privKeyFile.Close()
	err = gob.NewEncoder(privKeyFile).Encode(kh.privKey)
	if err != nil {
		return
	}
	return
}

// Decrypt will decrypt the data in the given byte array and return the result and, eventually, an error
func (kh *KeyHelper) Decrypt(b []byte) (res []byte, err error) {
	if kh.privKey == nil {
		err = generateAndSave(kh)
	}
	if err != nil {
		return
	}
	return rsa.DecryptPKCS1v15(rand.Reader, kh.privKey, b)
}

// PubKey will return the bytes of the server public key, encoded in x509 form.
// NOTE: doesn't return an error because it's convenient, might need to change
func (kh *KeyHelper) PubKey() []byte {
	if kh.pubKey == nil {
		generateAndSave(kh)
	}
	pubKey, _ := x509.MarshalPKIXPublicKey(kh.pubKey)
	return pubKey
}

func generateAndSave(kh *KeyHelper) error {
	err := kh.generate()
	if err != nil {
		return err
	}
	go func(kh *KeyHelper) {
		kh.save()
	}(kh)
	return err
}