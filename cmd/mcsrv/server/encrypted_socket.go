package server

import (
	"crypto/cipher"
	"io"
	"net"
)

type encryptedSocket struct {
	reader io.Reader
	writer io.Writer
}

func newEncryptedSocket(c net.Conn, encStream, decStream cipher.Stream) *encryptedSocket {
	return &encryptedSocket{
		reader: cipher.StreamReader{
			S: decStream,
			R: c,
		},
		writer: cipher.StreamWriter{
			S: encStream,
			W: c,
		},
	}
}

func (es *encryptedSocket) Read(b []byte) (int, error) {
	return es.reader.Read(b)
}

func (es *encryptedSocket) Write(b []byte) (int, error) {
	return es.writer.Write(b)
}
