// package server handles the lowest level of client - server communication, accepting new connections and forwarding
// packets to the registered handlers.
package server

import (
	"fmt"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol"
	"net"
)

// Server is a wrapper around net.Listener which handles client - server communication.
type Server struct {
	clients []*Connection // currently unused, might need it for "broadcasting" packets later
	ln      net.Listener
	// we're not handling down to specific packets because this allows for more specific handler functions
	// and to have structs like player.Player be able to handle packets when in a given State, instead of having
	// to register every single packet player.Player can handle
	// NOTE: might require a change from Handler to []Handler
	handlers map[protocol.State]Handler
}

// Handler describes the ability to handle Minecraft protocol packets coming from the server.Connection conn
type Handler interface {
	Handle(p protocol.Packet, conn *Connection)
}

// New returns a new Server with ln as the underlying tcp server.
func New(ln net.Listener) *Server {
	return &Server{
		clients: make([]*Connection, 1),
		ln:      ln,
	}
}

// Listen starts the server and accepts connections from clients.
// Doesn't spawn a goroutine, so a call to listen will block the goroutine it's called on.
func (s *Server) Listen() {
	for {
		l, err := s.ln.Accept()
		if err != nil {
			// TODO: properly handle error
			continue
		}
		conn := NewConnection(l, true)
		conn.Start()
		s.clients = append(s.clients, conn)
		go func(conn *Connection) {
			// TODO: cancelling?
			for {
				p, err := conn.ReceivePacket()
				if err != nil {
					// TODO: handle error
					fmt.Println("error receiving:", err)
					continue
				}
				if handler, ok := s.handlers[conn.State]; ok {
					// spawning a goroutine is up to the handler
					handler.Handle(p, conn)
				}
			}
		}(conn)
	}
}

// Handle registers the given Handler h for the protocol.State state
func (s *Server) Handle(state protocol.State, h Handler) {
	if s.handlers == nil {
		s.handlers = make(map[protocol.State]Handler)
	}
	s.handlers[state] = h
}

func (s *Server) HandleFunc(state protocol.State, f func(packet protocol.Packet, conn *Connection)) {
	s.Handle(state, HandlerFunc(f))
}

type HandlerFunc func(packet protocol.Packet, conn *Connection)

func (hf HandlerFunc) Handle(packet protocol.Packet, conn *Connection) {
	hf(packet, conn)
}
