package server

import (
	"bytes"
	"crypto/cipher"
	"fmt"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
	"net"
)

const PacketQueueSize = 25

// Connection handles communication with the client at the wire level
// it uses a packet queue both for receiving and sending packets in order to not slow down i/o because of packet processing overhead
type Connection struct {
	State           protocol.State
	Offline         bool
	conn            net.Conn
	socket          io.ReadWriter
	parseMap        protocol.ParseMap
	recvPacketQueue chan wirePacket
	sendPacketQueue chan wirePacket
}

type wirePacket struct {
	length codec.VarInt
	id     codec.VarInt
	data   []byte
}

// NewConnection return a *Connection with conn as the underlying communication method
func NewConnection(conn net.Conn, offline bool) *Connection {
	c := &Connection{
		State:           protocol.Handshaking,
		Offline:         offline,
		conn:            conn,
		socket:          conn,
		parseMap:        protocol.DefaultParseMap,
		recvPacketQueue: make(chan wirePacket, PacketQueueSize),
		sendPacketQueue: make(chan wirePacket, PacketQueueSize),
	}
	return c
}

//TODO: handle i/o errors, graceful shutdown (?)
func (c *Connection) receive() {
	for {
		var p wirePacket
		err := p.length.Decode(c.socket)
		if err != nil {
			continue
		}
		err = p.id.Decode(c.socket)
		if err != nil {
			continue
		}
		dataLen := int(p.length) - p.id.Length()
		p.data = make([]byte, dataLen)
		n, err := io.ReadFull(c.socket, p.data)
		if n != dataLen {
			err = fmt.Errorf("error reading packet: expected %d bytes, got %d", dataLen, n)
			fmt.Println(err)
			continue
		}
		c.recvPacketQueue <- p
	}
}

//TODO: handle i/o errors, graceful shutdown (?)
func (c *Connection) send() {
	for p := range c.sendPacketQueue {
		err := p.length.Encode(c.socket)
		if err != nil {
			continue
		}
		err = p.id.Encode(c.socket)
		if err != nil {
			continue
		}
		_, err = c.socket.Write(p.data)
	}
}

// Start starts the receiver and sender go-routines.
func (c *Connection) Start() {
	go c.receive()
	go c.send()
}

// Close closes the communication with the client
func (c *Connection) Close() {
	close(c.sendPacketQueue)
	close(c.recvPacketQueue)
	c.conn.Close()
}

// ReceivePacket pulls a wirePacket from the receiving channel, parses it
// and returns the specific type of packet based on the ID
// The new packet is returned as a protocol.Packet interface
func (c *Connection) ReceivePacket() (p protocol.Packet, err error) {
	rp := <-c.recvPacketQueue
	parser, ok := c.parseMap[c.State][rp.id]
	if !ok {
		err = fmt.Errorf("unknown packet type, State: %d id: %#x", c.State, rp.id)
		//p = &packet.Unknown{PacketID:rp.id, PacketState:codec.VarInt(c.State)}
		//err = p.Decode(bytes.NewReader(rp.data))
		return
	}
	p, err = parser(rp.data)
	return
}

// SendPacket converts a protocol.Packet to a wirePacket and passes it to the sending channel
// NOTE: will most likely need to write another SendPacketWithCallback function
func (c *Connection) SendPacket(p protocol.Packet) (err error) {
	wp := wirePacket{}
	buf := bytes.NewBuffer([]byte{})
	err = p.Encode(buf)
	if err != nil {
		return
	}
	wp.id = p.ID(false)
	wp.data = buf.Bytes()
	wp.length = codec.VarInt(wp.id.Length() + len(wp.data))
	c.sendPacketQueue <- wp
	return
}

// SetCipher encrypts the connection using encStream, decStream as ciphers
func (c *Connection) SetCipher(encStream, decStream cipher.Stream) {
	c.socket = newEncryptedSocket(c.conn, encStream, decStream)
}
