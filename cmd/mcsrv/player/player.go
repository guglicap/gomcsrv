package player

import "gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"

type Player struct {
	Username codec.String
}
