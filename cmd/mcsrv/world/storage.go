package world

type Chunk interface {
	Coordsable
}

type Block interface {
	Coordsable
}

type Storage interface {
	Chunk(x, z int) (Chunk, error)
	Block(x, y, z int) (Block, error)
	SetBlock(block Block) error
	SetChunk(chunk Chunk) error
}

type Coordsable interface {
	Coords() (x, y, z int)
}
