package world

import (
	"gitlab.com/guglicap/gomcsrv/cmd/mcsrv/player"
)

type World struct {
	players []player.Player
}

func New() *World {
	return &World{
		players: make([]player.Player, 0),
	}
}

func (w *World) AddPlayer(p player.Player) {
	w.players = append(w.players, p)
}
