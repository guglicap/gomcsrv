package protocol

// State represents the various connection states
type State int

const (
	Handshaking = iota
	Login
	Play
	Status
)
