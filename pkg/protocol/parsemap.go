package protocol

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
)

const (
	Version_1_14_4 = 498
)

// NOTE: might implement using reflection.
type ParseMap map[State]map[codec.VarInt]PacketParser

// DefaultParseMap maps the state and packet id to the parser for each serverbound packet
var DefaultParseMap = ParseMap{
	Handshaking: {
		0x00: ParseHandshake,
	},
	Login: {
		0x00: ParseLoginStart,
		0x01: ParseEncryptionResponse,
	},
	Play: {
		0x05: ParseClientSettings,
	},
}
