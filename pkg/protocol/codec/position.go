package codec

import (
	"encoding/binary"
	"io"
)

type Position struct {
	X, Y, Z int
}

func (v Position) Encode(w io.Writer) (err error) {
	pos := (uint64(v.X)&0x3FFFFFF)<<38 | (uint64(v.Z)&0x3FFFFFF)<<12 | uint64(v.Y)&0xFFF
	return binary.Write(w, binary.BigEndian, pos)
}

func (v *Position) Decode(r io.Reader) (err error) {
	var pos uint64
	err = binary.Read(r, binary.BigEndian, &pos)
	if err != nil {
		return
	}
	v.X = int(pos >> 38)
	v.Y = int(pos & 0xFFF)
	v.Z = int(pos << 26 >> 38)
	return
}
