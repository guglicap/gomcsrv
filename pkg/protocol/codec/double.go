package codec

import (
"encoding/binary"
"io"
)

type Double float64

func (v *Double) Decode(r io.Reader) (err error) {
	var a float64
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Double(a)
	return
}

func (v Double) Encode(w io.Writer) (err error) {
	a := float64(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
