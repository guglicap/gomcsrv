package codec

import (
	"encoding/binary"
	"io"
)

type Float float32

func (v *Float) Decode(r io.Reader) (err error) {
	var a float32
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Float(a)
	return
}

func (v Float) Encode(w io.Writer) (err error) {
	a := float32(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
