package codec

import (
	"errors"
	"io"
)

type VarInt int32

func (v *VarInt) Decode(r io.Reader) (err error) {
	n := 0
	result := int32(0)
	var read UByte
	for {
		err = read.Decode(r)
		value := read & 0x7f
		result |= int32(uint(value) << uint(7*n))
		n++
		if n > 5 {
			return errors.New("varint too big")
		}

		if (read & 0x80) == 0 {
			break
		}
	}
	*v = VarInt(result)
	return
}

func (v VarInt) Encode(w io.Writer) (err error) {
	num := uint32(v)
	for {
		b := num & 0x7F
		num >>= 7
		if num != 0 {
			b |= 0x80
		}
		err = UByte(b).Encode(w)
		if err != nil {
			return
		}
		if num == 0 {
			break
		}
	}
	return
}

func (v VarInt) Length() int {
	l := 0
	n := uint32(v)
	for {
		n >>= 7
		l++
		if n == 0 {
			break
		}
	}
	return l
}
