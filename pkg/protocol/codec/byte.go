//Package codec implements the various minecraft protocol types
package codec

import (
	"encoding/binary"
	"io"
)

type Byte int8

func (v *Byte) Decode(r io.Reader) (err error) {
	var a int8
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Byte(a)
	return
}

func (v Byte) Encode(w io.Writer) (err error) {
	a := int8(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
