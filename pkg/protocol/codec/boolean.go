package codec

import "io"

type Boolean bool

func (b *Boolean) Decode(r io.Reader) error {
	var a UByte
	err := a.Decode(r)
	if err != nil {
		return err
	}
	if a == 0x01 {
		*b = Boolean(true)
	} else {
		*b = Boolean(false)
	}
	return nil
}

func (b *Boolean) Encode(w io.Writer) error {
	var a UByte
	if bool(*b) {
		a = 0x01
	} else {
		a = 0x00
	}
	return a.Encode(w)
}
