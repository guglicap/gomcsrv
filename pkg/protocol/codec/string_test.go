package codec

import (
	"bytes"
	"testing"
)

func TestStringCodec(t *testing.T) {
	testString := "hello!"
	s := String(testString)
	b := bytes.NewBuffer([]byte{})
	err := s.Encode(b)
	if err != nil {
		t.Errorf("error encoding string '%s' : %v", testString, err)
	}
	x := String("")
	err = x.Decode(b)
	if err != nil {
		t.Errorf("error decoding string: %v", err)
	}
	if string(x) != testString {
		t.Errorf("string codec failed: expected '%s', got '%s'", testString, x)
	}
}
