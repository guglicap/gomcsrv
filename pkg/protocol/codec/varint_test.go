package codec

import (
	"bytes"
	"testing"
)

var varintSamples = map[int32][]byte {
	0 : {0x00},
	1 : {0x01},
	2 : {0x02},
	127 : {0x7f},
	128 : {0x80, 0x01},
	255 : {0xff, 0x01},
	2147483647 : {0xff, 0xff, 0xff, 0xff, 0x07},
	-1 : {0xff, 0xff, 0xff, 0xff, 0x0f},
	-2147483648: {0x80, 0x80, 0x80, 0x80, 0x08},
}

func TestVarInt_Decode(t *testing.T) {
	for v, b := range varintSamples {
		var a VarInt
		r := bytes.NewReader(b)
		err := a.Decode(r)
		if err != nil {
			t.Errorf("error decoding %v : %s", b, err)
		}
		if int32(a) != v {
			t.Errorf("incorrectly decoded %v : expected %d got %d", b, v, a)
		}
	}
}

func TestVarInt_Encode(t *testing.T) {
	for v, b := range varintSamples {
		a := VarInt(v)
		buf := bytes.NewBuffer([]byte{})
		err := a.Encode(buf)
		if err != nil {
			t.Errorf("error encoding %v : %s", b, err)
		}
		if ! bytes.Equal(b, buf.Bytes()) {
			t.Errorf("incorrectly encoded %d : expected %v got %v", v, b, buf.Bytes())
		}
	}
}

func TestVarInt_Length(t *testing.T) {
	for v, b := range varintSamples {
		a := VarInt(v)
		if a.Length() != len(b) {
			t.Errorf("wrong length for %d : expected %d, got %d", v, len(b), a.Length())
		}
	}
}
