package codec

import (
	"encoding/binary"
	"io"
)

type Short uint16

func (v *Short) Decode(r io.Reader) (err error) {
	var a uint16
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Short(a)
	return
}

func (v Short) Encode(w io.Writer) (err error) {
	a := uint16(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
