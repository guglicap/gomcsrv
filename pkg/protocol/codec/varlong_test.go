package codec

import (
	"bytes"
	"testing"
)

var varlongSamples = map[int64][]byte{
	0:                    {0x00},
	1:                    {0x01},
	2:                    {0x02},
	127:                  {0x7f},
	128:                  {0x80, 0x01},
	255:                  {0xff, 0x01},
	2147483647:           {0xff, 0xff, 0xff, 0xff, 0x07},
	-1:                   {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01},
	-2147483648:          {0x80, 0x80, 0x80, 0x80, 0xf8, 0xff, 0xff, 0xff, 0xff, 0x01},
	9223372036854775807:  {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f},
	-9223372036854775808: {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01},
}

func TestVarLong_Decode(t *testing.T) {
	for v, b := range varlongSamples {
		var a VarLong
		r := bytes.NewReader(b)
		err := a.Decode(r)
		if err != nil {
			t.Errorf("error decoding %v : %s", b, err)
		}
		if int64(a) != v {
			t.Errorf("incorrectly decoded %v : expected %d got %d", b, v, a)
		}
	}
}

func TestVarLong_Encode(t *testing.T) {
	for v, b := range varlongSamples {
		a := VarLong(v)
		buf := bytes.NewBuffer([]byte{})
		err := a.Encode(buf)
		if err != nil {
			t.Errorf("error encoding %v : %s", b, err)
		}
		if ! bytes.Equal(b, buf.Bytes()) {
			t.Errorf("incorrectly encoded %d : expected %v got %v", v, b, buf.Bytes())
		}
	}
}
