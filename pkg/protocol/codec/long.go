package codec

import (
	"encoding/binary"
	"io"
)

type Long int64


func (v *Long) Decode(r io.Reader) (err error) {
	var a int64
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Long(a)
	return
}

func (v Long) Encode(w io.Writer) (err error) {
	a := int64(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
