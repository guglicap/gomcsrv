package codec

import (
	"errors"
	"io"
)

type VarLong int64

func (v *VarLong) Decode(r io.Reader) (err error) {
	n := 0
	result := int64(0)
	var read UByte
	for {
		err = read.Decode(r)
		value := read & 0x7f
		result |= int64(uint64(value) << uint64(7*n))
		n++
		if n > 10 {
			return errors.New("varlong too big")
		}

		if (read & 0x80) == 0 {
			break
		}
	}
	*v = VarLong(result)
	return
}

func (v VarLong) Encode(w io.Writer) (err error) {
	num := uint64(v)
	for {
		b := num & 0x7F
		num >>= 7
		if num != 0 {
			b |= 0x80
		}
		err = UByte(b).Encode(w)
		if err != nil {
			return
		}
		if num == 0 {
			break
		}
	}
	return
}
