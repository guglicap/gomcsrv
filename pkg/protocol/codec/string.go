package codec

import (
	"errors"
	"fmt"
	"io"
)

type String string

func (v *String) Decode(r io.Reader) (err error) {
	var length VarInt
	var result string
	err = length.Decode(r)
	if err != nil {
		return
	}
	if length < 0 {
		err = errors.New(fmt.Sprintf("Decode, String length is below zero: %d", length))
		return
	}
	if length > 1048576 { // 2^(21-1)
		err = errors.New(fmt.Sprintf("Decode, String length is above maximum: %d", length))
		return
	}
	bytes := make([]byte, length)
	_, err = r.Read(bytes)
	if err != nil {
		return
	}
	result = string(bytes)
	*v = String(result)
	return
}

func (v String) Encode(w io.Writer) (err error) {
	bytes := []byte(v)
	err = VarInt(len(bytes)).Encode(w)
	if err != nil {
		return
	}
	_, err = w.Write(bytes)
	return
}

func (v String) String() string {
	return string(v)
}
