package codec

import "io"

type Codec interface {
	Decode(r io.Reader) error
	Encode(w io.Writer) error
}
