package codec

import (
	"encoding/binary"
	"io"
)

type UByte uint8

func (v *UByte) Decode(r io.Reader) (err error) {
	var a uint8
	err = binary.Read(r, binary.BigEndian, &a)
	*v = UByte(a)
	return
}

func (v UByte) Encode(w io.Writer) (err error) {
	a := uint8(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
