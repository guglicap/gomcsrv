package codec

import (
	"encoding/binary"
	"io"
)

type Int int32

func (v *Int) Decode(r io.Reader) (err error) {
	var a int32
	err = binary.Read(r, binary.BigEndian, &a)
	*v = Int(a)
	return
}

func (v Int) Encode(w io.Writer) (err error) {
	a := int32(v)
	err = binary.Write(w, binary.BigEndian, &a)
	return
}
