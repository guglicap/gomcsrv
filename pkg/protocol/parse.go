package protocol

import (
	"bytes"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/packet"
)

//PacketParser is a function that takes in packet data and returns a specific type of packet based on the id, conforming to the Packet interface
type PacketParser func([]byte) (Packet, error)

func decodePacket(b []byte, a Packet) error {
	r := bytes.NewReader(b)
	return a.Decode(r)
}

func ParseHandshake(b []byte) (Packet, error) {
	var a packet.Handshake
	err := decodePacket(b, &a)
	return &a, err
}

func ParseLoginStart(b []byte) (Packet, error) {
	var a packet.LoginStart
	err := decodePacket(b, &a)
	return &a, err
}

func ParseEncryptionResponse(b []byte) (Packet, error) {
	var a packet.EncryptionResponse
	err := decodePacket(b, &a)
	return &a, err
}

func ParseClientSettings(b []byte) (Packet, error) {
	var a packet.ClientSettings
	err := decodePacket(b, &a)
	return &a, err
}
