package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const LoginStartID = 0x00

type LoginStart struct {
	Username codec.String
}

func (l *LoginStart) Decode(r io.Reader) (err error) {
	decodeField(&l.Username, r, err)
	return
}

func (l *LoginStart) Encode(w io.Writer) (err error) {
	encodeField(&l.Username, w, err)
	return
}

func (l *LoginStart) ID(_ bool) codec.VarInt {
	return LoginStartID
}
