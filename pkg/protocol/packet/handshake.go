package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const HandshakeID = 0x00

type Handshake struct {
	ProtocolVersion codec.VarInt
	ServerAddress   codec.String
	ServerPort      codec.UShort
	NextState       codec.VarInt
}

func (h *Handshake) Decode(r io.Reader) (err error) {
	decodeField(&h.ProtocolVersion, r, err)
	decodeField(&h.ServerAddress, r, err)
	decodeField(&h.ServerPort, r, err)
	decodeField(&h.NextState, r, err)
	return
}

func (h *Handshake) Encode(w io.Writer) (err error) {
	err = ErrWrongDirectionServerbound
	return
}

func (h *Handshake) ID(_ bool) codec.VarInt {
	return HandshakeID
}
