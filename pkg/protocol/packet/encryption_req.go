package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const EncryptionRequestID = 0x01

type EncryptionRequest struct {
	ServerID          codec.String
	PubKeyLength      codec.VarInt
	PubKey            []byte
	VerifyTokenLength codec.VarInt
	VerifyToken       []byte
}

func NewEncryptionRequest(pubKey, verifyToken []byte) *EncryptionRequest {
	return &EncryptionRequest{
		ServerID:          "",
		PubKeyLength:      codec.VarInt(len(pubKey)),
		PubKey:            pubKey,
		VerifyTokenLength: codec.VarInt(len(verifyToken)),
		VerifyToken:       verifyToken,
	}
}

func (er *EncryptionRequest) ID(_ bool) codec.VarInt {
	return EncryptionRequestID
}

func (er *EncryptionRequest) Decode(r io.Reader) error {
	return ErrWrongDirectionClientbound
}

//TODO: check number bytes written, throw error in case of mismatch
func (er *EncryptionRequest) Encode(w io.Writer) (err error) {
	encodeField(&er.ServerID, w, err)
	encodeField(&er.PubKeyLength, w, err)
	_, err = w.Write(er.PubKey)
	if err != nil {
		return
	}
	encodeField(&er.VerifyTokenLength, w, err)
	_, err = w.Write(er.VerifyToken)
	return
}
