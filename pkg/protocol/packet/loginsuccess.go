package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const LoginSuccessID = 0x02

type LoginSuccess struct {
	UUID     codec.String
	Username codec.String
}

func NewLoginSuccess(uuid, username string) *LoginSuccess {
	return &LoginSuccess{
		UUID:     codec.String(uuid),
		Username: codec.String(username),
	}
}

func (ls *LoginSuccess) Decode(r io.Reader) error {
	return ErrWrongDirectionClientbound
}

func (ls *LoginSuccess) Encode(w io.Writer) (err error) {
	encodeField(&ls.UUID, w, err)
	encodeField(&ls.Username, w, err)
	return
}

func (ls *LoginSuccess) ID(_ bool) codec.VarInt {
	return LoginSuccessID
}
