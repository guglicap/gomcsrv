package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const JoinGameID = 0x25

type JoinGame struct {
	EntityID         codec.Int
	Gamemode         codec.UByte
	Dimension        codec.Int
	MaxPlayers       codec.UByte
	LevelType        codec.String
	ViewDistance     codec.VarInt
	ReducedDebugInfo codec.Boolean
}

func (jg *JoinGame) ID(_ bool) codec.VarInt {
	return JoinGameID
}

func (jg *JoinGame) Decode(r io.Reader) error {
	return ErrWrongDirectionClientbound
}

func (jg *JoinGame) Encode(w io.Writer) (err error) {
	encodeField(&jg.EntityID, w, err)
	encodeField(&jg.Gamemode, w, err)
	encodeField(&jg.Dimension, w, err)
	encodeField(&jg.MaxPlayers, w, err)
	encodeField(&jg.LevelType, w, err)
	encodeField(&jg.ViewDistance, w, err)
	encodeField(&jg.ReducedDebugInfo, w, err)
	return
}
