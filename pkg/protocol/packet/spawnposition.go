package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const SpawnPositionID = 0x4D

type SpawnPosition struct {
	Location codec.Position
}

func (s *SpawnPosition) ID(_ bool) codec.VarInt {
	return SpawnPositionID
}

func (s *SpawnPosition) Decode(r io.Reader) (err error) {
	err = ErrWrongDirectionClientbound
	return
}

func (s *SpawnPosition) Encode(w io.Writer) (err error) {
	encodeField(&s.Location, w, err)
	return
}
