// Package packet implements the various packets of the minecraft protocol
// Reference: https://wiki.vg/index.php?title=Protocol&oldid=15289
package packet

import (
	"errors"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

var (
	// ErrWrongDirectionServerbound is raised when calling Encode on a serverbound packet
	ErrWrongDirectionServerbound = errors.New("trying to encode a serverbound packet")
	// ErrWrongDirectionClientbound is raised when calling Decode on a clientbound packet
	ErrWrongDirectionClientbound = errors.New("trying to decode a clientbound packet")
)

// Holds packet IDs when a packet can be bound both ways, order is {serverbound, clientbound}
type CompoundID [2]codec.VarInt

func (c CompoundID) ID(sBound bool) codec.VarInt {
	if sBound {
		return c[0]
	}
	return c[1]
}

var decodeField = func(c codec.Codec, r io.Reader, err error) {
	if err != nil {
		return
	}
	err = c.Decode(r)
}

var encodeField = func(c codec.Codec, w io.Writer, err error) {
	if err != nil {
		return
	}
	err = c.Encode(w)
}
