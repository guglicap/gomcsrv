package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

var PlayerPositionAndLookID = CompoundID{0x12, 0x35}

type PlayerPositionAndLook struct {
	X, Y, Z    codec.Double
	Yaw, Pitch codec.Float
	Flags      codec.Byte
	TeleportID codec.VarInt
}

func (p *PlayerPositionAndLook) ID(sBound bool) codec.VarInt {
	return PlayerPositionAndLookID.ID(sBound)
}

func (p *PlayerPositionAndLook) Decode(r io.Reader) (err error) {
	err = ErrWrongDirectionClientbound
	return
}

func (p *PlayerPositionAndLook) Encode(w io.Writer) (err error) {
	encodeField(&p.X, w, err)
	encodeField(&p.Y, w, err)
	encodeField(&p.Z, w, err)
	encodeField(&p.Yaw, w, err)
	encodeField(&p.Pitch, w, err)
	encodeField(&p.Flags, w, err)
	encodeField(&p.TeleportID, w, err)
	return
}
