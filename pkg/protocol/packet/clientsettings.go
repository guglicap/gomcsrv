package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const ClientSettingsID = 0x05

type ClientSettings struct {
	Locale             codec.String
	ViewDistance       codec.Byte
	ChatMode           codec.VarInt
	ChatColors         codec.Boolean
	DisplayedSkinParts codec.UByte
	MainHand           codec.VarInt
}

func (p *ClientSettings) ID(_ bool) codec.VarInt {
	return ClientSettingsID
}

func (p *ClientSettings) Decode(r io.Reader) (err error) {
	decodeField(&p.Locale, r, err)
	decodeField(&p.ViewDistance, r, err)
	decodeField(&p.ChatMode, r, err)
	decodeField(&p.ChatColors, r, err)
	decodeField(&p.DisplayedSkinParts, r, err)
	decodeField(&p.MainHand, r, err)
	return
}

func (p *ClientSettings) Encode(w io.Writer) (err error) {
	err = ErrWrongDirectionServerbound
	return
}
