package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

var HeldItemChangeID = CompoundID{0x23, 0x3F}

type HeldItemChange struct {
	Slot codec.Byte
}

func (p *HeldItemChange) ID(sBound bool) codec.VarInt {
	return HeldItemChangeID.ID(sBound)
}

func (p *HeldItemChange) Decode(r io.Reader) (err error) {
	decodeField(&p.Slot, r, err)
	return
}

func (p *HeldItemChange) Encode(w io.Writer) (err error) {
	encodeField(&p.Slot, w, err)
	return
}
