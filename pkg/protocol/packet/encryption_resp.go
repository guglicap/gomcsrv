package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const EncryptionResponseID = 0x01

type EncryptionResponse struct {
	SharedSecretLength codec.VarInt
	SharedSecret       []byte
	VerifyTokenLength  codec.VarInt
	VerifyToken        []byte
}

func (er *EncryptionResponse) ID(_ bool) codec.VarInt {
	return EncryptionResponseID
}

//TODO: check number bytes read, throw error in case of mismatch
func (er *EncryptionResponse) Decode(r io.Reader) (err error) {
	decodeField(&er.SharedSecretLength, r, err)
	if err != nil {
		return
	}
	er.SharedSecret = make([]byte, er.SharedSecretLength)
	_, err = r.Read(er.SharedSecret)
	decodeField(&er.VerifyTokenLength, r, err)
	if err != nil {
		return
	}
	er.VerifyToken = make([]byte, er.VerifyTokenLength)
	_, err = r.Read(er.VerifyToken)
	return
}

func (er *EncryptionResponse) Encode(w io.Writer) (err error) {
	err = ErrWrongDirectionServerbound
	return
}
