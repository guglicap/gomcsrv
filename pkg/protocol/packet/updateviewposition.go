package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const UpdateViewPositionID = 0x40

type UpdateViewPosition struct {
	ChunkX, ChunkZ codec.VarInt
}

func (p *UpdateViewPosition) ID(_ bool) codec.VarInt {
	return UpdateViewPositionID
}

func (p *UpdateViewPosition) Decode(r io.Reader) (err error) {
	err = ErrWrongDirectionClientbound
	return
}

func (p *UpdateViewPosition) Encode(w io.Writer) (err error) {
	encodeField(&p.ChunkX, w, err)
	encodeField(&p.ChunkZ, w, err)
	return
}
