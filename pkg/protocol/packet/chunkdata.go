package packet

import (
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

const ChunkDataID = 0x21

type ChunkData struct {
	ChunkX, ChunkZ codec.Int
	FullChunk      codec.Boolean
	PrimaryBitMask codec.VarInt
	HeightMaps     []byte // nbt
	Size           codec.VarInt
	Data           struct {
		Data []struct {
			BlockCount      codec.Short
			BitsPerBlock    codec.UByte
			Palette         codec.Codec
			DataArrayLength codec.VarInt
			DataArray       []codec.Long
		}
	} // chunk data
	NumberBlockEntities codec.VarInt
	BlockEntities       []byte // array of nbt tags
}

func (c ChunkData) ID() codec.VarInt {
	return ChunkDataID
}

func (c ChunkData) Encode(w io.Writer) (err error) {
	encodeField(&c.ChunkX, w, err)
	encodeField(&c.ChunkZ, w, err)
	encodeField(&c.FullChunk, w, err)
	encodeField(&c.PrimaryBitMask, w, err)
	_, err = w.Write(c.HeightMaps)
	encodeField(&c.Size, w, err)
	for _, d := range c.Data.Data {
		encodeField(&d.BlockCount, w, err)
		encodeField(&d.BitsPerBlock, w, err)
		encodeField(d.Palette, w, err)
		encodeField(&d.DataArrayLength, w, err)
		for _, l := range d.DataArray {
			encodeField(&l, w, err)
		}
	}
	encodeField(&c.NumberBlockEntities, w, err)
	if err != nil {
		return
	}
	_, err = w.Write(c.BlockEntities)
	return
}

func (c ChunkData) Decode(r io.Reader) error {
	return ErrWrongDirectionClientbound
}
