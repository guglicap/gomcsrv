// package protocol describes the basic types needed to work with the Minecraft protocol
// this server is being developed for protocol version 498 (mc version 1.14.4)
package protocol

import (
	"errors"
	"gitlab.com/guglicap/gomcsrv/pkg/protocol/codec"
	"io"
)

var (
	ErrWrongPacketType = errors.New("received wrong packet type")
)

// Packet describes a Minecraft packet.
type Packet interface {
	// ID returns the Packet ID. Since some packets can be both serverbound and clientbound with different IDs
	// it needs a sBound boolean to know which ID to return. If true, returns the serverbound ID.
	ID(sBound bool) codec.VarInt
	// Decode fills the packet fields by reading from the reader r
	Decode(r io.Reader) error
	// Encode writes packet data to writer w
	Encode(w io.Writer) error
}
