// package nbt is a library for working with Minecraft nbt files.
// encoding is not supported yet.
package nbt

import (
	"io"
)

// Encoder handles nbt encoding.
type Encoder struct {
	w io.Writer
}

// NewEncoder returns an Encoder which will write the result of the encoding to w.
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{
		w: w,
	}
}

// Marshal will encode v to an array of bytes, eventually returning an error.
func Marshal(v interface{}) ([]byte, error) {

	return nil, nil
}

// Encode will encode v to the writer w specified in the constructor.
func (e *Encoder) Encode(v interface{}) error {
	return nil
}

func (e *Encoder) encode(v interface{}) error {

	return nil
}