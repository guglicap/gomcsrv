package nbt

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"errors"
	"gitlab.com/guglicap/gomcsrv/pkg/nbt/codec"
	"io"
	"reflect"
)

var (
	ErrInvalidUnmarshalType = errors.New("trying to unmarshal into wrong type")
)

// Decoder decodes reading reading from an underlying io.Reader
type Decoder struct {
	r io.Reader
}

// Unmarshal decodes data from the byte array b into the interface v, eventually returning an error.
func Unmarshal(b []byte, v interface{}) (err error) {
	d := &Decoder{
		r: bytes.NewReader(b),
	}
	return d.Decode(v)
}

// NewDecoder returns a Decoder with r as the underlying io.Reader
func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{
		r: r,
	}
}

// Decode decodes the data from the reader d.r into the interface v, eventually returning an error
func (d *Decoder) Decode(v interface{}) (err error) {
	header := make([]byte, 1)
	_, err = d.r.Read(header)
	if err != nil {
		return
	}
	if header[0] == 0x1f || header[0] == 0x78 { // compressed
		var compressedReader io.Reader
		mr := io.MultiReader(bytes.NewReader(header), d.r)
		if header[0] == 0x1f {
			compressedReader, err = gzip.NewReader(mr)
		} else {
			compressedReader, err = zlib.NewReader(mr)
		}
		if err != nil {
			return
		}
		_, err = compressedReader.Read(header)
		if err != nil {
			return
		}
		d.r = compressedReader
	}
	var t codec.Tag
	if header[0] == codec.TagCompound { // root compound
		// discard the root compound name
		var name codec.String
		err = name.Decode(d.r)
		if err != nil {
			return
		}
		a := new(codec.Compound)
		a.Init()
		t = a
	} else {
		t = codec.ParseTag(header[0])
	}
	if t == nil {
		return errors.New("nil root tag")
	}
	err = t.Decode(d.r)
	if err != nil {
		return
	}
	return d.assignValues(t, v)
}

// st must be the struct itself, not a pointer to it
func (d *Decoder) assignStruct(t codec.Compound, st reflect.Value) (err error) {
	for i := 0; i < st.NumField(); i++ {

		// get name for compound map based on struct tag / struct field name
		sf := st.Type().Field(i) // st.Type gets the struct's "metadata", .Field() returns the field "metadata"
		fn := sf.Name
		ft, ok := sf.Tag.Lookup("nbt")
		if ok {
			fn = ft
		}

		// get the codec.Tag from the compound map based on the name from above
		tag, ok := t[fn]
		if !ok {
			continue
		}
		f := st.Field(i) // get the struct field itself. this is settable
		err = d.assignValues(tag, f)
		if err != nil {
			break
		}
	}
	return
}

// list must be the slice itself, not a pointer to it
func (d *Decoder) assignList(nbtList []codec.Tag, list reflect.Value) (err error) {
	buf := reflect.MakeSlice(list.Type(), len(nbtList), len(nbtList))
	for i, t := range nbtList {
		err = d.assignValues(t, buf.Index(i))
		if err != nil {
			return
		}
	}
	list.Set(buf)
	return
}

// TODO: might need tidying up
func (d *Decoder) assignValues(t codec.Tag, v interface{}) (err error) {
	var ve reflect.Value
	if reflect.TypeOf(v) == reflect.TypeOf(reflect.Value{}) {
		ve = v.(reflect.Value)
	} else {
		ve = reflect.ValueOf(v)
	}
	if ve.Kind() == reflect.Ptr {
		ve = ve.Elem()
	}
	if !ve.CanSet() {
		return errors.New("v is not settable, cannot unmarshal")
	}
	vk := ve.Kind()
	switch tag := t.(type) {
	case *codec.Byte:
		if vk == reflect.Uint8 || vk == reflect.Uint || vk == reflect.Uint16 || vk == reflect.Uint32 || vk == reflect.Uint64 {
			ve.SetUint(uint64(*tag))
		} else if vk == reflect.Int8 || vk == reflect.Int || vk == reflect.Int16 || vk == reflect.Int32 || vk == reflect.Int64 {
			ve.SetInt(int64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Long:
		if vk == reflect.Uint64 || vk == reflect.Uint {
			ve.SetUint(uint64(*tag))
		} else if vk == reflect.Int64 || vk == reflect.Int {
			ve.SetInt(int64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Short:
		if vk == reflect.Uint16 || vk == reflect.Uint32 || vk == reflect.Uint64 || vk == reflect.Uint {
			ve.SetUint(uint64(*tag))
		} else if vk == reflect.Int16 || vk == reflect.Int32 || vk == reflect.Int64 || vk == reflect.Int {
			ve.SetInt(int64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Int:
		if vk == reflect.Uint32 || vk == reflect.Uint64 || vk == reflect.Uint {
			ve.SetUint(uint64(*tag))
		} else if vk == reflect.Int32 || vk == reflect.Int64 || vk == reflect.Int {
			ve.SetInt(int64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Float:
		if vk == reflect.Float32 || vk == reflect.Float64 {
			ve.SetFloat(float64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Double:
		if vk == reflect.Float64 {
			ve.SetFloat(float64(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.ByteArray:
		if ve.Type() == reflect.TypeOf([]byte(*tag)) {
			ve.SetBytes([]byte(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.IntArray:
		if ve.Type() == reflect.TypeOf([]int32(*tag)) {
			l := len(*tag)
			buf := reflect.MakeSlice(ve.Type(), l, l)
			for i, a := range *tag {
				buf.Index(i).SetInt(int64(a))
			}
			ve.Set(buf)
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.LongArray:
		if ve.Type() == reflect.TypeOf([]int64(*tag)) {
			l := len(*tag)
			buf := reflect.MakeSlice(ve.Type(), l, l)
			for i, a := range *tag {
				buf.Index(i).SetInt(a)
			}
			ve.Set(buf)
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.String:
		if vk == reflect.String {
			ve.SetString(string(*tag))
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.List:
		if vk == reflect.Slice {
			err = d.assignList(*tag, ve)
		} else {
			err = ErrInvalidUnmarshalType
		}
	case *codec.Compound:
		if vk == reflect.Struct {
			err = d.assignStruct(*tag, ve)
		} else {
			err = ErrInvalidUnmarshalType
		}
	default:
		err = ErrInvalidUnmarshalType
	}
	return
}
