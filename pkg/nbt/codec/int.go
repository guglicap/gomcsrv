package codec

import (
	"encoding/binary"
	"io"
)

type Int int32

func (b *Int) Encode(w io.Writer) (err error) {
	err = binary.Write(w, binary.BigEndian, b)
	return
}

func (b *Int) Decode(r io.Reader) (err error) {
	err = binary.Read(r, binary.BigEndian, b)
	return
}
