package codec

import (
	"encoding/binary"
	"io"
)

type Float float32

func (b *Float) Encode(w io.Writer) (err error) {
	err = binary.Write(w, binary.BigEndian, b)
	return
}

func (b *Float) Decode(r io.Reader) (err error) {
	err = binary.Read(r, binary.BigEndian, b)
	return
}
