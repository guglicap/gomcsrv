package codec

import "io"

type List []Tag

func (l *List) Decode(r io.Reader) (err error) {
	var elemType Byte
	err = elemType.Decode(r)
	if err != nil {
		return
	}
	var length Int
	err = length.Decode(r)
	if err != nil {
		return
	}
	*l = make([]Tag, length)
	for i := range *l {
		(*l)[i] = ParseTag(byte(elemType))
		err = (*l)[i].Decode(r)
		if err != nil {
			return
		}
	}
	return
}

func (l *List) Encode(w io.Writer) (err error) {
	return
}
