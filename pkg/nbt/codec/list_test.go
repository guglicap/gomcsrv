package codec

import (
	"bytes"
	"testing"
)

func TestList_Decode(t *testing.T) {
	b := []byte{TagByte, 0x00, 0x00, 0x00, 0x05, 0x01, 0x02, 0x03, 0x04, 0x05}
	var l List
	r := bytes.NewReader(b)
	err := l.Decode(r)
	if err != nil {
		t.Error(err)
	}
}
