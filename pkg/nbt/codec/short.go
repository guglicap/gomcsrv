package codec

import (
	"encoding/binary"
	"io"
)

type Short int16

func (b *Short) Encode(w io.Writer) (err error) {
	err = binary.Write(w, binary.BigEndian, b)
	return
}

func (b *Short) Decode(r io.Reader) (err error) {
	err = binary.Read(r, binary.BigEndian, b)
	return
}
