package codec

import (
	"encoding/binary"
	"io"
)

type IntArray []int32

func (b *IntArray) Decode(r io.Reader) (err error) {
	var length Int
	err = length.Decode(r)
	if err != nil {
		return
	}
	*b = make([]int32, length)
	err = binary.Read(r, binary.BigEndian, *b)
	return
}

func (b *IntArray) Encode(w io.Writer) (err error) {
	length := Int(len(*b))
	err = length.Encode(w)
	if err != nil {
		return
	}
	err = binary.Write(w, binary.BigEndian, *b)
	return
}
