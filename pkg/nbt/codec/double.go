package codec

import (
	"encoding/binary"
	"io"
)

type Double float64

func (b *Double) Encode(w io.Writer) (err error) {
	err = binary.Write(w, binary.BigEndian, b)
	return
}

func (b *Double) Decode(r io.Reader) (err error) {
	err = binary.Read(r, binary.BigEndian, b)
	return
}
