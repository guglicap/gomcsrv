package codec

import (
	"errors"
	"io"
)

type errorCodec string

func (e *errorCodec) Decode(r io.Reader) (err error) {
	return errors.New(string(*e))
}

func (e *errorCodec) Encode(w io.Writer) (err error) {
	return errors.New(string(*e))
}
