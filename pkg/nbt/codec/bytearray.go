package codec

import (
	"io"
)

type ByteArray []byte

func (b *ByteArray) Decode(r io.Reader) (err error) {
	var length Int
	err = length.Decode(r)
	if err != nil {
		return
	}
	*b = make([]byte, length)
	_, err = r.Read(*b)
	return
}

func (b *ByteArray) Encode(w io.Writer) (err error) {
	length := Int(len(*b))
	err = length.Encode(w)
	if err != nil {
		return
	}
	_, err = w.Write(*b)
	return
}
