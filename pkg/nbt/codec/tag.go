package codec

import "io"

// thanks github.com/Tnze/go-mc
const (
	TagEnd byte = iota
	TagByte
	TagShort
	TagInt
	TagLong
	TagFloat
	TagDouble
	TagByteArray
	TagString
	TagList
	TagCompound
	TagIntArray
	TagLongArray
)

type Tag interface {
	Encode(w io.Writer) error
	Decode(r io.Reader) error
}

func ParseTag(id byte) (t Tag) {
	switch id {
	case TagByte:
		var b Byte
		t = &b
	case TagByteArray:
		var b ByteArray
		t = &b
	case TagDouble:
		var b Double
		t = &b
	case TagFloat:
		var b Float
		t = &b
	case TagInt:
		var b Int
		t = &b
	case TagList:
		var b List
		t = &b
	case TagLong:
		var b Long
		t = &b
	case TagShort:
		var b Short
		t = &b
	case TagString:
		var b String
		t = &b
	case TagCompound:
		var b Compound
		b.Init()
		t = &b
	case TagIntArray:
		var b IntArray
		t = &b
	case TagLongArray:
		var b LongArray
		t = &b
	case TagEnd:
		b := errorCodec("unexpected TagEnd")
		t = &b
	default:
		b := errorCodec("invalid tag id, check compression")
		t = &b
	}
	return
}
