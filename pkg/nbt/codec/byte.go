package codec

import (
	"encoding/binary"
	"io"
)

type Byte int8

func (b *Byte) Encode(w io.Writer) (err error) {
	err = binary.Write(w, binary.BigEndian, b)
	return
}

func (b *Byte) Decode(r io.Reader) (err error) {
	err = binary.Read(r, binary.BigEndian, b)
	return
}
