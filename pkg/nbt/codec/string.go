package codec

import "io"

type String string

// NOTE: doc on wiki.vg mentions modified UTF8, we're using standard here.

func (s *String) Encode(w io.Writer) (err error) {
	length := Short(len(*s))
	err = length.Encode(w)
	if err != nil {
		return
	}
	_, err = w.Write([]byte(*s))
	return
}

func (s *String) Decode(r io.Reader) (err error) {
	var length Short
	err = length.Decode(r)
	if err != nil {
		return
	}
	buf := make([]byte, length)
	_, err = r.Read(buf)
	*s = String(buf)
	return
}
