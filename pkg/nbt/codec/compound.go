package codec

import "io"

type Compound map[string]Tag

func (c *Compound) Init() {
	*c = make(map[string]Tag)
}

func (c *Compound) Decode(r io.Reader) (err error) {
	for {
		var id Byte
		err = id.Decode(r)
		if err != nil {
			return
		}
		if byte(id) == TagEnd {
			break
		}
		var name String
		err = name.Decode(r)
		if err != nil {
			return
		}
		t := ParseTag(byte(id))
		err = t.Decode(r)
		if err != nil {
			return
		}
		(*c)[string(name)] = t
	}
	return
}

func (c *Compound) Encode(w io.Writer) (err error) {
	return
}
